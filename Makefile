install-prerequisites:
	npm install -g yarn
	npm install -g gulp

install:
	-mkdir node_modules
	chmod 777 node_modules
	npm install
	bundle install
	yarn --ignore-engines
	yarn upgrade
	touch install

run: install
	-chmod -R 777 dist
	gulp

develop:
	docker-compose up

clean:
	-docker-compose stop
	-docker-compose rm -f
	-rm -rf dist
	-rm yarn.lock
	-rm -rf node_modules
	-rm install

test: install
	npm test